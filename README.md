# Graph-copy

Graph-copy is a library for (de)serializing arbitrary Clean expressions.

## Maintainer & license

This project is maintained by [TOP Software][].

It is licensed under the BSD-2-Clause license; for details, see the [LICENSE](/LICENSE) file.

[TOP Software]: https://top-software.nl
