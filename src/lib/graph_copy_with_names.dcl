definition module graph_copy_with_names;

/**
 * This provides serialisation of arbitrary expressions. Deserialisation is possible using executables containing the
 * same symbols and such symbols are type-compatible. This does even work between executables of different
 * architectures.
 */

import StdString,_SystemStrictLists;
from symbols_in_program import ::Symbol;

:: DescInfo = {di_prefix_arity_and_mod :: !Int, di_name :: !{#Char}};

 /**
  * String representation of an arbitrary expression.
  * @param The expression to serialise.
  * @result The serialised expression (first result) with pointers to descriptors replaced by references to the
  *         `DescInfo` array (second result). The `DescInfo` values contain references (`di_prefix_arity_and_mod`) to
  *         module names (third argument).
  */
copy_to_string_with_names :: a -> (!*{#Char},!*{#DescInfo},!*{#String});

/**
 * Deserialisation of an arbitrary expression serialised using {{`copy_to_string_with_names`}}.
 * @param The serialised expression (see result of {{`copy_to_string_with_names`}}).
 * @param The list of descriptors (see result of {{`copy_to_string_with_names`}}).
 * @param The list of module names (see result of {{`copy_to_string_with_names`}}).
 * @param The symbol table obtained using `symbols_in_program.read_symbols`.
 * @result The deserialised expression and the constant `0`, which can be used to force evaluation of the
 *         deserialized expression.
 */
copy_from_string_with_names :: !*{#Char} !*{#DescInfo} !*{#String} !{#Symbol} -> (.a,!Int);

// Internal functions (normally you should not use them directly):
replace_desc_numbers_by_descs :: !Int !*{#Char} !{#Int} !Int !Int -> *{#Char};
make_symbol_name :: !String !String !Int -> String;
