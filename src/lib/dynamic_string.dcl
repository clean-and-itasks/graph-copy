definition module dynamic_string

/**
 * This module is deprecated as it provides a subset of the functionality provided by `graph_copy`.
 */

import StdDynamic
import graph_copy

//* This function is deprecated. Use {{`graph_copy.copy_to_string`}} instead.
dynamic_to_string 	:: !Dynamic -> *{#Char}

//* This function is deprecated. Use {{`graph_copy.copy_from_string`}} instead.
string_to_dynamic 	:: *{#Char} -> .Dynamic
