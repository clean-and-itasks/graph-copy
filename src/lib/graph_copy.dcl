definition module graph_copy

/**
 * This provides serialisation of arbitrary expressions. Deserialisation is however only possible for the exact same
 * executable. This is mainly useful for communication between subprocesses and threads.
 */

 /**
  * String representation of an arbitrary expression.
  * @param The expression to serialise.
  * @result The serialised expression.
  */
copy_to_string :: !.a -> *{#Char}

/**
 * Deserialisation of an arbitrary expression serialised using `copy_to_string`. If the expression was not serialised
 * using the exact same executable or the type does not match the serialised value, the behaviour is undefined.
 * (The program probably crashes with a segmentation fault.)
 * @param The serialised expression.
 * @result The deserialised expression and the constant `0`, which can be used to force evaluation of the
 *         deserialized expression.
 */
copy_from_string :: !*{#Char} -> (.a,!Int)
