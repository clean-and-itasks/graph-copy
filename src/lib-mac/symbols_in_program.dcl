definition module symbols_in_program;

from StdFile import ::Files;
import _SystemStrictLists;

:: Symbol = { symbol_name :: !String, symbol_value :: !Int};

/**
 * Reads the symbol table from a Clean executable.
 * This requires the program to be compiled with the following options:
 * ```
 * clm_options:
 *     generate_descriptors: true
 *     export_local_labels: true
 *     strip: false
 * ```
 * Otherwise, the result is an empty array.
 * @param The path to the executable (the function aborts if the file does not exist).
 * @param A file environment.
 * @result The symbol table.
 */
read_symbols :: !{#Char} !*Files -> (!{#Symbol},!*Files);

/**
 * Does a binary search for a symbol in the sorted array of symbols.
 * @param The symbol to search for.
 * @param The symbol array.
 * @result The index of the symbol to search for, `-1` if the symbol is not found.
 */
get_symbol_value :: !{#Char} !{#Symbol} -> Int;
