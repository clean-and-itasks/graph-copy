# Changelog

#### 3.0.5

- Fix: bug in copy graph to string that occurs if the garbage collector has to be called if a node
       (record or constructor) of size > 2 with non pointers cannot be copied (maybe crashes only if > 2 pointers)

#### 3.0.4

- Chore: support base `3.0`.

#### 3.0.3

- Chore: allow base-rts 3.

#### 3.0.2

- Chore: support base `v2.0`.

#### 3.0.1

- Fix: use correct bsr instruction on 32 bit systems.

## 3.0.0 (Broken on 32 bit)

- Feature: adapt graph-copy to new array representations.

#### 2.0.2

- Documentation: add documentation to DCLs.

#### 2.0.1

- Fix: base-rts dependency.

## 2.0.0

- BROKEN DUE TO BAD BASE-RTS DEPENDENCY!
- Update git repo location and license.

#### 1.0.1

- Feature: add windows-x64 support.

## v1.0.0

- First tagged version.
